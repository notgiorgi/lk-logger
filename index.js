const winston = require('winston')
require('winston-daily-rotate-file')

const moment = require('moment-timezone')
const util = require('util')

const timestamp = () => moment(new Date()).tz('Asia/Tbilisi').format('Y-MM-DD HH:mm:ss')

const formatter = (options) => {
    const timestamp = options.timestamp()
    const level = options.level.toUpperCase()
    const msg = options.message ? options.message : ''
    const meta = options.meta && Object.keys(options.meta).length ? `${JSON.stringify(options.meta)}` : ''

    return `[${timestamp} | ${level}] ${msg}${meta ? `\n${meta}` : '' }`
}

/**
 * @typedef LoggerConfig
 * @type {Object}
 * @property {string} datePattern
 * @property {string} filename
 */
/**
 * Creates a logger
 * 
 * @param {LoggerConfig} loggerConfig
 * 
 * @returns {winston.Logger}
 */
function createLogger(loggerConfig) {
    const defaultConfig = {
        json: false,
        timestamp,
        formatter,

        // Rotation config
        datePattern: 'yyyy-MM-dd_',
        prepend: true,
        localTime: true,
    }

    // extends default config
    const newConfig = Object.assign({}, defaultConfig, loggerConfig)

    return new (winston.Logger)({
        level: 'debug',
        transports: [
            new (winston.transports.Console)({
                timestamp,
                formatter,
            }),
            new (winston.transports.DailyRotateFile)(newConfig),
        ],
    })
}

module.exports = createLogger
